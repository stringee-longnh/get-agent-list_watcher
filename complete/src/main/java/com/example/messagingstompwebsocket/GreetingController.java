package com.example.messagingstompwebsocket;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

import java.util.Arrays;
import java.util.List;

@Controller
public class GreetingController {



	@MessageMapping("/hello")
	@SendTo("/topic/greetings")
	public String updateAgentList(String agentList) throws Exception {
		List<String> givenAgents = Arrays.asList(agentList.split("\\|"));
		GlobalVariables.agentList = givenAgents;
		return String.join("", givenAgents);
	}


}
