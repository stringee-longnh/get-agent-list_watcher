package com.example.messagingstompwebsocket;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {


    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;

    @RequestMapping("/get-agents")
    public String getAgents(@RequestBody String payload) throws Exception {
        String result = addAgentListToPayload(GlobalVariables.agentList, payload);
        brokerMessagingTemplate.convertAndSend("/topic/greetings", payload);
        return result;
    }

    private String addAgentListToPayload(List<String> agentList, String payload) {
        String result;
        JSONObject queue = new JSONObject(payload);
        JSONArray calls = (JSONArray) queue.get("calls");
        for (int i = 0; i < calls.length(); i++) {
            JSONObject call = calls.getJSONObject(i);
            JSONArray agentArray = getAgentArray(GlobalVariables.agentList);
            call.put("agents", agentArray);
        }

        queue.put("calls", calls);

        return queue.toString();
    }

    private JSONArray getAgentArray(List<String> agentList) {
        JSONArray result = new JSONArray();
        for (String agent : agentList) {
            JSONObject jAgent = new JSONObject();
            jAgent.put("stringee_user_id", agent);
            jAgent.put("routing_type", 1); // 1 means rout to App/Sip phone
            jAgent.put("answer_timeout", 60);

            result.put(jAgent);
        }
        return result;
    }
}
