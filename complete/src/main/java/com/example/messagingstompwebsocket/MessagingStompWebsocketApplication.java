package com.example.messagingstompwebsocket;

import jdk.nashorn.internal.objects.Global;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class MessagingStompWebsocketApplication {

	static void init() {
		GlobalVariables.agentList = new ArrayList<>();
	}
	public static void main(String[] args) {
		init();
		SpringApplication.run(MessagingStompWebsocketApplication.class, args);
	}
}
