### Lưu ý: project get-list-agents_watcher ở trong thư mục complete

# Các tính năng:
1. Tiếp nhận webhook get list agent (giống như cách mà server của khách hàng tiếp nhận)
2. Config agent để phản hồi về cho Stringee Server (StringeeServer sẽ đổ cuộc gọi đến đúng agent được config ở đây)
3. Hiển thị danh sách các request từ Stringee Server (phục vụ debug)

# các bước cài đặt:
1. clone project này
2. cd vào thư mục complete
3. chạy lệnh: `./gradlew build -x test`
4. copy file jar ở thư mục: `build/libs/abcdef.jar`
5. khởi chạy project bằng lệnh: `java -jar abcdef.jar` (chỉ định đúng dường dẫn)
6. truy cập localhost:8080 & enjoy